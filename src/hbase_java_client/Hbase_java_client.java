/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbase_java_client;

import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

/**
 *
 * @author caca
 */
public class Hbase_java_client {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        org.apache.hadoop.conf.Configuration configuration = org.apache.hadoop.hbase.HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(configuration);

        String cachh = configuration.get("hbase.client.scanner.caching");
        System.out.println("try to connect..." + cachh);
        System.out.println("try to connect..." + configuration.get("hbase.client.scanner.timeout.period"));
        System.out.println("try to connect..." + configuration.get("hbase.client.operation.timeout"));
        System.out.println("try to connect..." + configuration.get("hbase.rpc.timeout"));

        System.out.println("try to connect hadoop..." + configuration.get("fs.defaultFS"));
        System.out.println("try to connect hadoop..." + configuration.get("dfs.client.socket-timeout"));
        System.out.println("try to connect hadoop..." + configuration.get("dfs.namenode.name.dir"));

        String TABLE_NAME = "POLY_SIX_TABLE";
        Admin admin = connection.getAdmin();
        TableName tableName = TableName.valueOf(TABLE_NAME);
        Table table = connection.getTable(tableName);
        Scan s = new Scan();
        s.setBatch(Integer.MAX_VALUE);
        s.setMaxResultSize(Long.MAX_VALUE);
        s.setCaching(50000);
        ResultScanner rs = table.getScanner(s);
//        String qualifier = null;
//        String columnfamily = null;
        int countresult=0;
        int countcell = 0;

        double size = 0;
        long all_cell_size = 0;
long startTime = System.nanoTime();
        for (Result result : rs) { //8 turns
            countresult++;
            for (Cell cell : result.rawCells()) { //8 turns
                
                KeyValue keyValue = (KeyValue) cell;
                all_cell_size = all_cell_size + keyValue.heapSize();
                countcell++;
            }
        }
        long stopTime = System.nanoTime();
        
        System.out.println("testing answer : " + all_cell_size);
        System.out.println(" result size : " + countresult);
        System.out.println(" cell size : " + countcell);
        System.out.println(stopTime - startTime);
       
    }

}
